// Copyright 2017 Janez Je�, Denis Lempl, Niko Pavlinek, �iga Sever
//
// This file is part of Orp.
//
// Orp is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Orp is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
// License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Orp.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include "ECS.hpp"

class Position_Component : public Component
{
private:
    int xpos;
    int ypos;
    int x1 = 0;
    int y1 = 0;
public :

    Position_Component() {};
    Position_Component(int x, int y)
    {
        x1 = x;
        y1 = y;
    }

    int x() { return xpos; }
    int y() { return ypos; }

    void init() override
    {
        xpos = x1;
        ypos = y1;
    }

    void update() override
    {

    }

    void set_pos(int x, int y)
    {
        xpos = x;
        ypos = y;
    }
};

class Sprite_Component : public Component
{
private:
    Position_Component * position;
    SDL_Texture *texture;
    SDL_Rect src_rect, dest_rect;

public:

    Sprite_Component() = default;
    Sprite_Component(const char* path)
    {
        set_texture(path);
    }

    void set_texture(const char* path)
    {
        texture = TextureManager::load_texture(path);
    }

    void init() override
    {
        position = &entity->get_component<Position_Component>();

        src_rect.x = src_rect.y = 0;
        src_rect.w = src_rect.h = 64;
        dest_rect.w = dest_rect.h = 64;
    }
    void update() override
    {
        dest_rect.x = position->x();
        dest_rect.y = position->y();
    }

    void draw() override
    {
        TextureManager::draw(texture, src_rect, dest_rect);
        //logger.log(WARNING, "rise component");
    }
};