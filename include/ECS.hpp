// Copyright 2017 Janez Je�, Denis Lempl, Niko Pavlinek, �iga Sever
//
// This file is part of Orp.
//
// Orp is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Orp is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
// License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Orp.  If not, see <http://www.gnu.org/licenses/>.

//Entity Component System

#pragma once

#include <vector>
#include <memory>
#include <algorithm>
#include <bitset>
#include <array>

#include "Logger.hpp"

class Component;
class Entity;


//vsakemu da id
using ComponentID = std::size_t;

inline ComponentID get_component_typeID()
{
    static ComponentID lastID = 0;
    return lastID++;                //vsakic ko se klice funkcija dodeli vaskic drugemu id
}


template <typename T> 
inline ComponentID get_component_typeID () noexcept
{
    static_assert (std::is_base_of<Component, T>::value, "");
    static ComponentID typeID = get_component_typeID ();
    return typeID;
}

constexpr std::size_t max_components = 32;

using ComponentBitSet = std::bitset<max_components>;
using ComponentArray = std::array<Component*, max_components>;


class Component {
private:

public:
    Entity *entity;

    virtual void init () {}
    virtual void update () {}
    virtual void draw () {}
    
    virtual ~Component () {}
};



class Entity {
private:
    bool active = true;
    std::vector<std::unique_ptr<Component>> components;

    ComponentArray component_array;
    ComponentBitSet component_bitset;

public:
    void  update(void)
    {
        for (auto& c : components) c->update ();
    }

    void draw (void) 
    {
        for (auto& c : components) c->draw();
    }

    bool is_active (void) const { return active; }
    void destroy (void) { active = false; }

    template <typename T> bool has_component () const
    {
        return component_bitset[get_component_typeID<T>()];
    }

    template<typename T, typename... TArgs>
    T& add_component(TArgs&&...mArgs)
    {
        T* c(new T(std::forward<TArgs>(mArgs)...));
        c->entity = this;
        std::unique_ptr<Component> uPtr{ c };
        components.emplace_back(std::move(uPtr));

        component_array[get_component_typeID<T>()] = c;
        component_bitset[get_component_typeID<T>()] = true;

        c->init();
        return *c;
    }
    template <typename T> T& get_component() const
    {
        auto ptr(component_array[get_component_typeID<T>()]);
        return *static_cast<T*>(ptr);
    }

};


class Manager
{
private:
    std::vector<std::unique_ptr<Entity>> entities;
public:
    void update()
    {
        for (auto &e : entities)
            e->update();
    }
    void  draw()
    {
        for (auto &e : entities)
            e->draw();
    }
    void refresh()
    {
        entities.erase(std::remove_if(std::begin(entities), std::end(entities), 
            [](const std::unique_ptr<Entity> &mEntety)
        {
            return !mEntety->is_active();
        }),
            std::end(entities));
    }

    Entity& add_entity()
    {
        Entity *e = new Entity();
        std::unique_ptr<Entity> uPtr{ e };
        entities.emplace_back(std::move(uPtr));
        return *e;
    }

};
