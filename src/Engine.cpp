// Copyright 2017 Janez Jež, Denis Lempl, Niko Pavlinek, Žiga Sever
//
// This file is part of Orp.
//
// Orp is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Orp is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
// License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Orp.  If not, see <http://www.gnu.org/licenses/>.



#include "Engine.hpp"

#include "Global.hpp"
#include "TextureManager.hpp"
#include "Map.hpp"

#include "ECS.hpp"
#include "Components.hpp"

//nevem ampak to more tu bit ker drugace je neki wierd eror
Map *map;
Manager manager;
auto& enemy1(manager.add_entity());
auto& enemy2(manager.add_entity());
int lvl1[20][25] = {
    { 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,6,7, },
    { 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,8,9, },
    { 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, },
    { 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, },
    { 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, },
    { 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, },
    { 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, },
    { 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, },
    { 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, },
    { 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, },
    { 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, },
    { 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, },
    { 1,1,1,4,1,1,1,3,1,1,5,1,1,1,1,1,3,1,1,5,1,4,1,1,1, },
    { 2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2, },
    { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, },
    { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, },
    { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, },
    { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, },
    { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, },
    { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, }

};

int lvl2[20][25] = {
    { 10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10, },
    { 10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10, },
    { 10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10, },
    { 10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10, },
    { 10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10, },
    { 11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11, },
    { 11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11, },
    { 11,11,11,11,11,11,11,12,11,11,11,11,12,11,11,11,11,12,11,11,11,11,11,11,11, },
    { 11,11,11,11,11,11,11,13,11,11,11,11,13,11,11,11,11,13,11,11,11,11,11,11,11, },
    { 11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11, },
    { 11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11, },
    { 11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11, },
    { 11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11, },
    { 10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10, },
    { 10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10, },
    { 10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10, },
    { 10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10, },
    { 10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10, },
    { 10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10, },
    { 10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10, }
};


Engine::Engine (void)
    : is_running (true), window (NULL)
{
}

Engine::~Engine (void)
{
}

//
// Inicializira Orp Window
//
void
Engine::set_state (const bool running)
{
    is_running = running;
}

bool
Engine::get_state (void) const
{
    return is_running;
}

SDL_Renderer *
Engine::get_renderer (void) const
{
    return renderer;
}

int
Engine::get_win_width (void) const
{
    return width;
}

int
Engine::get_win_height (void) const
{
    return height;
}

bool map1;
bool enemy1F = true;
bool enemy2F = true;
void
Engine::init (const char* title, int xpos, int ypos, int width, int height,
              bool fullscreen)
{
    this->width = width;
    this->height = height;

    int flags = 0;

    if (fullscreen)
        flags = SDL_WINDOW_FULLSCREEN;

    if (SDL_Init (SDL_INIT_EVERYTHING) != 0)
        is_running = false;
    else
    {
        logger.log (INFO, "Subsystem Initialised.");

        window = SDL_CreateWindow (title, xpos, ypos, width, height, flags);
        this->width = width;
        this->height = height;

        if (window)
            logger.log (INFO, "Window Created.");

        renderer = SDL_CreateRenderer (window, -1,0);
        if (renderer)
        {
            SDL_SetRenderDrawColor (renderer, 255, 255, 255, 255);
            logger.log (INFO, "Renderer Created.");
        }

        is_running = true;
    }

    player.set_rectsize (64);   // velikost player sprita (64 = 64x64)
    player.set_texture ("../sprites/Opr2.png");
    map = new Map ();
    map1 = true;
    map->load_map(lvl1);


    enemy1.add_component<Position_Component>(4*32, engine.get_win_height() - 9 * 32);
    //enemy1.get_component<Position_Component>().set_pos(250, 250);
    enemy1.add_component<Sprite_Component>("../sprites/zombieR.png");

    enemy2.add_component<Position_Component>(13 * 32, engine.get_win_height() - 9 * 32);
    enemy2.add_component<Sprite_Component>("../sprites/skeleton_spriteR.png");


}

//
// Upravlja dogodke, sprejema user input
//
void
Engine::handle_events (void)
{
    SDL_Event event;
    while (SDL_PollEvent (&event) != 0)
    {
        if (event.type == SDL_QUIT)
            is_running = false;
    }

    // TODO: better comment
    // Postavi hitrost igralca na 0, da se preneha premikati.
    player.set_velx (0);
    // player.set_vely (0);

    const Uint8 *keyboard_state = SDL_GetKeyboardState (NULL);

    if (keyboard_state[SDL_SCANCODE_Q])
    {
        SDL_Event quit;
        quit.type = SDL_QUIT;
        SDL_PushEvent (&quit);
    }
    else if (keyboard_state[SDL_SCANCODE_1])
    {
        map->load_map(lvl1);
        enemy1.get_component<Sprite_Component>().set_texture("../sprites/zombieR.png");
        enemy2.get_component<Sprite_Component>().set_texture("../sprites/skeleton_spriteR.png");
        enemy1F = true;
        enemy2F = true;

        map1 = true;
    }
    else if (keyboard_state[SDL_SCANCODE_2])
    {
        map->load_map(lvl2);
        enemy1.get_component<Sprite_Component>().set_texture("../sprites/mage_idleR.png");
        enemy2.get_component<Sprite_Component>().set_texture("../sprites/dwarf_idle2R.png");
        enemy1F = true;
        enemy2F = true;
        map1 = false;
    }
    else if (keyboard_state[SDL_SCANCODE_UP])
    {
        player.set_vely (player.get_vely () - player.get_rectsize () / 10);
    }
    else if (keyboard_state[SDL_SCANCODE_LEFT])
    {
        player.set_velx (player.get_velx () - player.get_rectsize () / 10);
    }
    else if (keyboard_state[SDL_SCANCODE_RIGHT])
    {
        player.set_velx (player.get_velx () + player.get_rectsize () / 10);
    }
}



//
// Posodobi pozicije objektov
//
void
Engine::update (void)
{
    player.update ();
    manager.update();

    //enemy movment
    if (map1)
    {
        if (enemy1F)
        {
            enemy1.get_component<Position_Component>().set_pos(enemy1.get_component<Position_Component>().x() + 1, engine.get_win_height() - 9 * 32);
            if ((enemy1.get_component<Position_Component>().x() >= 32 * 9))
            {
                enemy1.get_component<Sprite_Component>().set_texture("../sprites/zombieL.png");
                enemy1F = false;
            }
        }
        else {
            enemy1.get_component<Position_Component>().set_pos(enemy1.get_component<Position_Component>().x() - 1, engine.get_win_height() - 9 * 32);
            if ((enemy1.get_component<Position_Component>().x() <= 33 * 4))
            {
                enemy1.get_component<Sprite_Component>().set_texture("../sprites/zombieR.png");
                enemy1F = true;
            }
        }
        //enemy2
        if (enemy2F)
        {
            enemy2.get_component<Position_Component>().set_pos(enemy2.get_component<Position_Component>().x() + 2, engine.get_win_height() - 9 * 32);
            if ((enemy2.get_component<Position_Component>().x() >= 32 * 20))
            {
                enemy2F = false;
                enemy2.get_component<Sprite_Component>().set_texture("../sprites/skeleton_spriteL.png");
            }
        }
        else {
            enemy2.get_component<Position_Component>().set_pos(enemy2.get_component<Position_Component>().x() - 2, engine.get_win_height() - 9 * 32);
            if ((enemy2.get_component<Position_Component>().x() <= 33 * 13))
            {
                enemy2F = true;
                enemy2.get_component<Sprite_Component>().set_texture("../sprites/skeleton_spriteR.png");

            }
        }
    }
    else
    {
        if (enemy1F)
        {
            enemy1.get_component<Position_Component>().set_pos(enemy1.get_component<Position_Component>().x() + 1, engine.get_win_height() - 9 * 32);
            if ((enemy1.get_component<Position_Component>().x() >= 32 * 9))
            {
                enemy1.get_component<Sprite_Component>().set_texture("../sprites/mage_idleL.png");
                enemy1F = false;
            }
        }
        else {
            enemy1.get_component<Position_Component>().set_pos(enemy1.get_component<Position_Component>().x() - 1, engine.get_win_height() - 9 * 32);
            if ((enemy1.get_component<Position_Component>().x() <= 33 * 4))
            {
                enemy1.get_component<Sprite_Component>().set_texture("../sprites/mage_idleR.png");
                enemy1F = true;
            }
        }
        //enemy2
        if (enemy2F)
        {
            enemy2.get_component<Position_Component>().set_pos(enemy2.get_component<Position_Component>().x() + 2, engine.get_win_height() - 9 * 32);
            if ((enemy2.get_component<Position_Component>().x() >= 32 * 20))
            {
                enemy2F = false;
                enemy2.get_component<Sprite_Component>().set_texture("../sprites/dwarf_idle2L.png");
            }
        }
        else {
            enemy2.get_component<Position_Component>().set_pos(enemy2.get_component<Position_Component>().x() - 2, engine.get_win_height() - 9 * 32);
            if ((enemy2.get_component<Position_Component>().x() <= 33 * 13))
            {
                enemy2F = true;
                enemy2.get_component<Sprite_Component>().set_texture("../sprites/dwarf_idle2L.png");

            }
        }
    }
    ///

}

//
// Prikaze objekte na zaslon
//
void
Engine::render (void) const
{
    SDL_RenderClear (renderer);

    //tukaj dodavaj stvari da se renderajo
    map->draw_map();
    manager.draw();

    SDL_RenderCopy (renderer, player.get_texture (), NULL, player.get_rect ());

    SDL_RenderPresent (renderer);
}

//
// Pocisti okno ko se igra zapre (cleanup)
//
void
Engine::clean (void) const
{
    SDL_DestroyWindow (window);
    SDL_DestroyRenderer (renderer);
    SDL_Quit ();
    logger.log (INFO, "Game Cleaned.");
}
