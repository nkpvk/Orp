// Copyright 2017 Janez Jež, Denis Lempl, Niko Pavlinek, Žiga Sever
//
// This file is part of Orp.
//
// Orp is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Orp is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
// License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Orp.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <string>

#include "SDL2/SDL.h"

#include "Engine.hpp"
#include "Global.hpp"
#include "Player.hpp"

static const float min_vely = -12.0f;
static const float max_vely = 10.0f;
static const float gravity = 0.5f;

Player::Player (void)
    : texture (NULL), velx (0), vely (0), frame (0)
{
    rect = new SDL_Rect;

    // XXX: Temporarily put player sprite at origin
    rect->x = 0;
    rect->y = 340;
}

Player::~Player (void)
{
    delete rect;
}

void
Player::set_texture (const std::string sprite)
{
    texture = TextureManager::load_texture (sprite.c_str ());
}

SDL_Texture *
Player::get_texture (void) const
{
    return texture;
}

SDL_Rect *
Player::get_rect (void) const
{
    return rect;
}

void
Player::set_rectsize (int rectsize)
{
    this->rectsize = this->rect->h = this->rect->w = rectsize;
}

int
Player::get_rectsize (void) const
{
    return rectsize;
}

void
Player::set_velx (const float velx)
{
    this->velx = velx;
}

float
Player::get_velx (void) const
{
    return velx;
}

void
Player::set_vely (const float vely)
{
    this->vely = vely;
}

float
Player::get_vely (void) const
{
    return vely;
}

void
Player::update (void)
{
    this->frame++;
    this->rect->x += velx;

    if ((this->rect->x < 0)
        || (this->rect->x + rectsize > (engine.get_win_width ())))
        this->rect->x -= velx;

    this->vely += gravity;
    this->rect->y += this->vely;

    // Največja navpična hitrost, ki jo lahko igralec doseže.
    if (this->vely >= max_vely)
        this->vely = max_vely;

    if ((this->rect->y < 0)
        || (this->rect->y + rectsize > engine.get_win_height () - 7 * 32))
        this->rect->y -= this->vely;

    if (this->vely < min_vely)
        this->vely = 0;

    // Igralec se premika navzgor za 1 frame, nato začne nanj vplivati
    // gravitacija.
    if (this->frame == 1 && this->vely < 0)
        this->frame = this->vely = 0;
}

void
Player::set_x (const int x)
{
    rect->x = x;
}

int
Player::get_x (void) const
{
    return rect->x;
}

void
Player::set_y (const int y)
{
    rect->y = y;
}

int
Player::get_y (void) const
{
    return rect->y;
}
