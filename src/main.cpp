// Copyright 2017 Janez Jež, Denis Lempl, Niko Pavlinek, Žiga Sever
//
// This file is part of Orp.
//
// Orp is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Orp is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
// License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Orp.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <string>

#include "SDL2/SDL.h"

#include "Engine.hpp"
#include "Global.hpp"
#include "Logger.hpp"

int
main (int argc, char **argv)
{
    const int FPS = 60;
    const int frame_delay = 1000 / FPS;

    unsigned int frame_start;
    int frame_time;

    engine.init ("Orp", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                 800,640, false);

    while (engine.get_state ()) // Poglej ali je Engine pripravljen
    {
        frame_start = SDL_GetTicks ();

        engine.handle_events ();
        engine.update ();
        engine.render ();

        frame_time = SDL_GetTicks () - frame_start;

        if (frame_delay > frame_time)
            SDL_Delay (frame_delay - frame_time);
    }

    engine.clean ();
    return 0;
}
